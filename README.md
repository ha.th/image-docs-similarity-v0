# image-docs-similarity-v0
# image-docs-similarity-v0
## Overview
- *Visual-image-search* is a simple image-based image search engine using Keras + Flask. You can launch the search engine just by running two python scripts.
- `offline.py`: This script extracts deep features from images. Given a set of database images, a 4096D fc6-feature is extracted for each image using the VGG16 network with ImageNet pre-trained weights.
- `servergo.py`: This script runs a web-server. You can send your query image to the server via a Flask web-intereface. Then relevant images to the query are retrieved by the simple nearest neighbor search
### Technology
- tensorflow==1.15.0
- keras==2.1.4
- flask
- python 3
### Environment Setup
```sh
$ cd visual-image-search
$ pip install -r requirements.txt
```
### Training dataset
- Before run server must run `offline.py`

### How to run
```sh
python servergo.py
```
## Testing via APIs

----
  Returns json data about similar matches.

* **URL**

  /recognize

* **Method:**

  `POST`
  
*  **Headers**

   **Required:**
 
   `Content-Type=application/json`
**Response**
```
{
    "details": [
        {
            "label": "33. Đề nghị cấp hạn mức bất thường VPSecurities",
            "name": "030058_MG_T98_22-08-2019_22-22.pdf",
            "path": "030058_MG_T98_22-08-2019_22-22.pdf.jpg",
            "score": "0.0"
        },
        {
            "label": "33. Đề nghị cấp hạn mức bất thường VPSecurities",
            "name": "030258_MG_T98_22-08-2019_3-3.pdf",
            "path": "030258_MG_T98_22-08-2019_3-3.pdf.jpg",
            "score": "0.17659907"
        },
        {
            "label": "33. Đề nghị cấp hạn mức bất thường VPSecurities",
            "name": "030286_MG_T98_22-08-2019_22-22.pdf",
            "path": "030286_MG_T98_22-08-2019_22-22.pdf.jpg",
            "score": "0.17778188"
        }
    ]
}
```
